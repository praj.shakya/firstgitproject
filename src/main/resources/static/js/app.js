$(document).ready(function () {
    $('.date-picker').datepicker({
        format: 'yyyy-mm-dd'
    });
});


function searchTable(params) {
    var content = '';
    $.each(params.coll, function (index, row) {

        if (match(row, params.keyword)) {
            content += params.template;
            console.log(content);
            for (attr in row) {
                content = content.replace('{{' + attr + '}}', row[attr]);
            }
        }
    });
    $(params.el).html(content);

    function match(row, param) {
        for (attr in row) {

            if (row[attr].toString().toLowerCase().includes(param.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}