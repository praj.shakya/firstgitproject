package com.prajwal.code.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers().permitAll()
                .anyRequest()
                .authenticated().and()
                .formLogin().loginPage("/login").permitAll().and()
                .logout().permitAll();
                
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/webjars/**");
    }

    

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        
        BCryptPasswordEncoder encoder=getPasswordEncoder();
        
        auth.inMemoryAuthentication().passwordEncoder(encoder)
                .withUser("admin")
                .password(encoder.encode("admin1234"))
                .authorities("ADMIN");
    }
    
  public BCryptPasswordEncoder getPasswordEncoder(){
      return new BCryptPasswordEncoder();
  }
    
}
