package com.prajwal.code.controller;



import com.prajwal.code.entity.Customer;
import com.prajwal.code.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/customers")
public class CustomerController extends CRUDController<Customer, Integer> {
    @Autowired
    CustomerRepository rep;
    public CustomerController() {
        viewPath="customers";
        pathUri="customers";
        pageTitle="CUSTOMERS";
    }
    @GetMapping(value = "/active")
    public String active(Model model){
        model.addAttribute("records", rep.getActiveCustomers());
        return viewPath+"/index";
    }
    @GetMapping(value = "/inactive")
    public String inactive(Model model){
        model.addAttribute("records", rep.getInactiveCustomers());
        return viewPath+"/index";
    }
}
