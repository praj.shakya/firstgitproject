package com.prajwal.code.controller;

import com.prajwal.code.entity.Sale;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/sales")
public class SaleController extends CRUDController<Sale, Integer>{

    public SaleController() {
        viewPath="sales";
        pathUri="sales";
        pageTitle="SOLD ITEMS";
    }
    
}
