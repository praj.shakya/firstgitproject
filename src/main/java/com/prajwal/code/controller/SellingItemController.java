package com.prajwal.code.controller;

import com.prajwal.code.entity.SellingItem;
import com.prajwal.code.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/sellingitems")
public class SellingItemController extends CRUDController<SellingItem, Integer>{

    @Autowired
    private ItemRepository itemrepository;
    
    public SellingItemController() {
        viewPath="sellingitems";
        pathUri="sellingitems";
        pageTitle="SALE ITEMS";
    }

    @Override
    public String create(Model model) {
        model.addAttribute("items", itemrepository.findAll());
        return super.create(model); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String edit(@PathVariable("id")Integer id, Model model) {
        model.addAttribute("items", itemrepository.findAll());
        return super.edit(id, model); //To change body of generated methods, choose Tools | Templates.
    }
    
}
