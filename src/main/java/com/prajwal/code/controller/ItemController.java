package com.prajwal.code.controller;

import com.prajwal.code.entity.Item;
import com.prajwal.code.repository.ItemRepository;
import com.prajwal.code.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/items")
public class ItemController extends CRUDController<Item, Integer>{
    
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private SupplierRepository supplierRepository;
    public ItemController() {
        viewPath="items";
        pathUri="items";
        pageTitle="ITEMS";
    }

    //Rewriting the create method to send the supplier_id in items table from suppliers.
    @Override
    public String create(Model model) {
        model.addAttribute("suppliers", supplierRepository.findAll());
        return super.create(model); 
    }

    @Override
    public String edit(@PathVariable("id")Integer id, Model model) {
        model.addAttribute("suppliers", supplierRepository.findAll());
        return super.edit(id, model); 
    }
    
    
}
