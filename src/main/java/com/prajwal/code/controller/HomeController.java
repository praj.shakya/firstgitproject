
package com.prajwal.code.controller;

import com.prajwal.code.entity.Customer;
import com.prajwal.code.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = "/")
public class HomeController extends SiteController{

   
    @Autowired
    public CustomerRepository cr;
    @GetMapping
        public String index(Model model){
            model.addAttribute("active", cr.getActiveCustomers());
        return "index";
    }
}
