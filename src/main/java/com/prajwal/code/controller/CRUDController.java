/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prajwal.code.controller;

import com.prajwal.code.repository.CustomerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Prajwal
 */

public abstract class CRUDController<T, ID> extends SiteController{
    @Autowired
    private JpaRepository<T, ID> repository;
    @Autowired
    private CustomerRepository cr;
    
    
    @GetMapping
    public String index(Model model){
        model.addAttribute("records", repository.findAll());
        return viewPath+"/index";
    }
    
    @GetMapping(value = "/create")
    public String create(Model model){
        return pathUri+"/create";
    }
    @PostMapping()
    public String save(T model){
        repository.save(model);
        return "redirect:/"+pathUri;
    }
    
    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id")ID id, Model model){
        model.addAttribute("record", repository.findById(id).get());
        return pathUri+"/edit";
    }
    
    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id")ID id){
        repository.deleteById(id);
        return "redirect:/"+pathUri;
    }
    
    @CrossOrigin(origins = "*")
    @GetMapping(value = "/json")
    @ResponseBody
    public List<T> json(){
        return repository.findAll();
         
    }
}
