
package com.prajwal.code.controller;

import org.springframework.web.bind.annotation.ModelAttribute;


public class SiteController {
    protected String pageTitle="HOMEPAGE";
    protected String viewPath;
    protected String pathUri;
    
    @ModelAttribute(value = "viewPath")
    public String getviewPath(){
        return viewPath;
    }
    @ModelAttribute(value = "pathUri")
    public String getpathUri(){
        return pathUri;
    }
    
    @ModelAttribute(value = "pageTitle")
    public String getpageTitle(){
        return pageTitle;
    }
}
