package com.prajwal.code.controller;




import com.prajwal.code.entity.Staff;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/staffs")
public class StaffController extends CRUDController<Staff, Integer>{

    public StaffController() {
        viewPath="staffs";
        pathUri="staffs";
        pageTitle="STAFFS";
    }
    
}
