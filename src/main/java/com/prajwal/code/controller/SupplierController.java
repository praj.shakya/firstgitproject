package com.prajwal.code.controller;

import com.prajwal.code.entity.Supplier;
import com.prajwal.code.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/suppliers")
public class SupplierController extends CRUDController<Supplier, Integer>{
    @Autowired
    private SupplierRepository rep;
    
    public SupplierController() {
        viewPath="suppliers";
        pathUri="suppliers";
        pageTitle="SUPPLIERS";
    }
    @GetMapping(value = "/active")
    public String active(Model model){
        model.addAttribute("records", rep.getActiveSuppliers());
        return viewPath+"/index";
    }
    @GetMapping(value = "/inactive")
    public String inactive(Model model){
        model.addAttribute("records", rep.getInactiveSuppliers());
        return viewPath+"/index";
    }
}
