package com.prajwal.code.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_selling_items")
public class SellingItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "selling_price")
    private int selling_price;
    @Column(name = "completed_date")
    private Date completed_date;
    @Column(name = "availability")
    private boolean availability;
    @Column(name = "size")
    private String size;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "item_detail")
    private String item_detail;
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    @ManyToOne
    private Item item_id;
    

    public SellingItem() {
    }

    public SellingItem(int id) {
        this.id = id;
    }

    public SellingItem(int id, int selling_price, Date completed_date, boolean availability, String size, int quantity, String item_detail, Item item_id) {
        this.id = id;
        this.selling_price = selling_price;
        this.completed_date = completed_date;
        this.availability = availability;
        this.size = size;
        this.quantity = quantity;
        this.item_detail = item_detail;
        this.item_id = item_id;
    }

    public Item getItem_id() {
        return item_id;
    }

    public void setItem_id(Item item_id) {
        this.item_id = item_id;
    }

    
    
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getItem_detail() {
        return item_detail;
    }

    public void setItem_detail(String item_detail) {
        this.item_detail = item_detail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSelling_price() {
        return selling_price;
    }

    public void setSelling_price(int selling_price) {
        this.selling_price = selling_price;
    }

    public Date getCompleted_date() {
        return completed_date;
    }

    public void setCompleted_date(Date completed_date) {
        this.completed_date = completed_date;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

}
