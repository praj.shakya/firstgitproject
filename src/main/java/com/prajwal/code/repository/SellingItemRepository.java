package com.prajwal.code.repository;

import com.prajwal.code.entity.SellingItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellingItemRepository extends JpaRepository<SellingItem, Integer>{
    
}
