package com.prajwal.code.repository;

import com.prajwal.code.entity.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
    @Query(value = "select * from tbl_customers where status=1", nativeQuery = true)
    List<Customer> getActiveCustomers();
    @Query(value = "select * from tbl_customers where status=0", nativeQuery = true)
    List<Customer> getInactiveCustomers();
}
