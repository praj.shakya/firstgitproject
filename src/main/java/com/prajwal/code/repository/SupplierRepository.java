package com.prajwal.code.repository;

import com.prajwal.code.entity.Supplier;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Integer>{
    @Query(value = "select * from tbl_suppliers where status=1", nativeQuery = true)
    List<Supplier> getActiveSuppliers();
    @Query(value = "select * from tbl_suppliers where status=0", nativeQuery = true)
    List<Supplier> getInactiveSuppliers();
}
